<!-- .slide: data-state="nologo" -->

<svg width="100%" height="100%" version="1.1" viewBox="0 0 95.124 83.862" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g transform="translate(-33.173 -58.569)"><rect x="102.46" y="79.087" width="24.584" height="18.023" fill="#ba0c2f" style="paint-order: markers stroke;" class="ncclogo-1"></rect><path d="m61.616 71.426v32.462l10.545 6.0881 10.547-6.0895v-32.491m-33.796 0.06092v39.933l23.391 13.505 23.567-13.606v-40.01m-60.197 7.8123v39.37l36.52 21.085 36.598-21.13v-13.759m-36.57-33.037v-8.3566m-36.54 8.3391v-10.532h72.783v10.394" fill="none" stroke="#194893" stroke-width="5" class="ncclogo-2"></path><path d="m109.13 79.077v6.7889h-6.6782c-3e-3 5.22e-4 -6e-3 0.0011-9e-3 0.0016v4.6235c3e-3 5.11e-4 6e-3 1e-3 9e-3 0.0015h6.6782v6.6234c1e-3 0.0066 3e-3 0.0131 4e-3 0.01964h4.6163c1e-3 -0.0065 3e-3 -0.01308 4e-3 -0.01964v-6.6234h13.291c0.0236-4e-3 0.047-0.0086 0.0703-0.01395v-4.5987c-0.0233-0.0053-0.0467-0.01-0.0703-0.01395h-13.291v-6.7889zm2.312 8.226c0.11649 0.42693 0.45002 0.76046 0.87695 0.87695-0.42641 0.11612-0.75982 0.44875-0.87695 0.87488-0.11713-0.42613-0.45054-0.75876-0.87695-0.87488 0.42693-0.11649 0.76046-0.45002 0.87695-0.87695z" fill="#fff" style="paint-order: markers stroke;" class="ncclogo-3"></path><path d="m110.38 79.081v8.0347h-7.9272v2.126h7.9272v7.8739h2.126v-7.8739h14.541v-2.126h-14.541v-8.0347z" color="#000000" fill="#00205b" stroke-linecap="square" stroke-linejoin="round" style="paint-order: markers stroke;" class="ncclogo-4"></path></g></svg>
<!-- .element: class="r-stretch" -->

# Hello World

This is the *first* first **first**  slide

---

## Second slide

- Bullet point one
- And another claim! 
- With a [link](#)

Down here also


Note: 
Available by pressing '**S**' in the presentation. Notes are separated like - newline and a line saying 'Note:'


---

<!-- .slide: data-background-color="black" -->

## Hello in the dark!

---

![image](https://images.unsplash.com/photo-1632200714041-8de62b2b435b?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NTczfHxyYWluYm93fGVufDB8fDB8fHww)
<!-- .element: class="r-stretch" -->

Test

---

<!-- .slide: data-background="https://images.unsplash.com/photo-1559694940-f3b2ae9a1f6b?q=80&w=2940&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" data-background-opacity="1"-->

## Nice, a background!


---

<!-- .slide: data-background-video="https://cdn.coverr.co/videos/coverr-popcorn-flying-from-a-box-with-a-red-background-9217/1080p.mp4" data-background-video-loop="true" -->


🫢 HTML in .md

<div class="halfsplit">
<div class="gridleft"> 

### Half
Grid left

</div>
<div class="gridright">

### Split
Grid right

</div>
</div>

--


🫢 HTML in .md

<div class="thirdsplit">
<div class="gridleft"> 

### Two thirds
Grid left

</div>
<div class="gridright">

### One third
Grid right

</div>
</div>

--

🫢 HTML in .md

<div class="threes">
<div> 

<i class="ph-fill ph-crown-simple"></i>

One

</div>
<div>

<i class="ph-fill ph-scroll"></i>

Two

</div>
<div>

<i class="ph-fill ph-users-three"></i>

Three

</div>
</div>

--

<!-- .slide: data-background-image="https://images.unsplash.com/photo-1550353175-a3611868086b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" -->

Wow, is this text on a blurred box?
<!-- .element: class="boxed" -->


---

## [Fork away](https://gitlab.com/nccno/prestemplate/)

---

<!-- .slide: data-state="nologo" -->

<svg width="100%" height="100%" version="1.1" viewBox="0 0 95.124 83.862" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g transform="translate(-33.173 -58.569)"><rect x="102.46" y="79.087" width="24.584" height="18.023" fill="#ba0c2f" style="paint-order: markers stroke;" class="ncclogo-1"></rect><path d="m61.616 71.426v32.462l10.545 6.0881 10.547-6.0895v-32.491m-33.796 0.06092v39.933l23.391 13.505 23.567-13.606v-40.01m-60.197 7.8123v39.37l36.52 21.085 36.598-21.13v-13.759m-36.57-33.037v-8.3566m-36.54 8.3391v-10.532h72.783v10.394" fill="none" stroke="#194893" stroke-width="5" class="ncclogo-2"></path><path d="m109.13 79.077v6.7889h-6.6782c-3e-3 5.22e-4 -6e-3 0.0011-9e-3 0.0016v4.6235c3e-3 5.11e-4 6e-3 1e-3 9e-3 0.0015h6.6782v6.6234c1e-3 0.0066 3e-3 0.0131 4e-3 0.01964h4.6163c1e-3 -0.0065 3e-3 -0.01308 4e-3 -0.01964v-6.6234h13.291c0.0236-4e-3 0.047-0.0086 0.0703-0.01395v-4.5987c-0.0233-0.0053-0.0467-0.01-0.0703-0.01395h-13.291v-6.7889zm2.312 8.226c0.11649 0.42693 0.45002 0.76046 0.87695 0.87695-0.42641 0.11612-0.75982 0.44875-0.87695 0.87488-0.11713-0.42613-0.45054-0.75876-0.87695-0.87488 0.42693-0.11649 0.76046-0.45002 0.87695-0.87695z" fill="#fff" style="paint-order: markers stroke;" class="ncclogo-3"></path><path d="m110.38 79.081v8.0347h-7.9272v2.126h7.9272v7.8739h2.126v-7.8739h14.541v-2.126h-14.541v-8.0347z" color="#000000" fill="#00205b" stroke-linecap="square" stroke-linejoin="round" style="paint-order: markers stroke;" class="ncclogo-4"></path></g></svg>
<!-- .element: class="r-stretch" -->
